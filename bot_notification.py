#!/usr/bin/env python3

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import logging
from dynamodb_check import scan_table_allpages
from ec2_check import check_ec2
from sqs_check import check_sqs
import json
import os


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    filename="output.log", filemode='a',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def ec2(bot, update):
    raw_json = check_ec2()
    update.message.reply_text(json.dumps(raw_json, indent=1))


def status(bot, update):
    data_dict, user_dict = scan_table_allpages('active_image')
    update.message.reply_text(json.dumps(data_dict, indent=1))
    update.message.reply_text(json.dumps(user_dict, indent=1))


def sqs(bot, update):
    msq = check_sqs()
    update.message.reply_text(json.dumps(msq, indent=1))


def help(bot, update):
    update.message.reply_text("Hello Hy name is Harris, commands: ['/status', '/ec2', '/sqs']")


if __name__ == '__main__':
    updater = Updater(os.environ['TELEGRAM_KEY'])
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("status", status))
    dp.add_handler(CommandHandler("ec2", ec2))
    dp.add_handler(CommandHandler("sqs", sqs))
    dp.add_handler(CommandHandler("help", help))
    updater.start_polling()
