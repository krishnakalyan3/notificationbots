#!/usr/bin/env python3

from sessions import boto_session

sqs = boto_session.resource('sqs')
queue = sqs.get_queue_by_name(QueueName='filename')


def check_sqs():
    attrib = queue.attributes.items()
    for i in list(attrib):
        if i[0] == "ApproximateNumberOfMessages":
            return '{} Messages in the queue'.format(i[1])


if __name__ == '__main__':
    print(check_sqs())
